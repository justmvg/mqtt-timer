***MQTT Timer***

# MQTT-Timer

Tasmota firmware comes with a feature for timers to automatically turn on / off or toggle your device. Unfortunately it's impossible to set a timer in minutes. (e.g. automatically turn off after 60 minutes)

This NodeJS script listens when your device turns on and will turn it off on a given interval in the configuration.

## Prerequisites

- Node installed

## Set-up

- Clone repository
- run `npm install`
- edit `app.cfg` to your needs
- run `node app.js` or start `pm2-start.yml` if using pm2

## Optional configuration
If you would like to use the button on the sonoff device, a configuration change should be applied to publish a message on button press.
In the tasmota webinterface, navigate to the console and use the command `ButtonTopic <name>`. The topic will be `cmnd/<name>/POWER`.