const CfgParser =   require('configparser');
const cfgfile = './app.cfg';

class Config extends CfgParser{
    g(section, key) {
        this.read(cfgfile);
        this.sections();
        return this.get(section, key);
    }
}

module.exports = Config;