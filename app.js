const mqtt = require('mqtt');
const cron = require('node-schedule');
const Config = require('./config');
const cfg = new Config();

const mqttBroker =  cfg.g('MQTT', 'broker');
const mqttClient =  cfg.g('MQTT', 'id');
const mqttTopic =  cfg.g('MQTT', 'topic');
const mqttInfo =  cfg.g('MQTT', 'state');
const scheduleMin = parseInt(cfg.g('SCHEDULE', 'minutes'));

const client = mqtt.connect(mqttBroker, {clientId: mqttClient});

client.on('connect', () => {
  var welcometext = `Client ${mqttClient} connected`;
  console.log(welcometext)
  client.publish(mqttInfo, welcometext)
  client.subscribe(mqttInfo, (err) => {
    if (!err) { 
      console.log(`Subscribed to ${mqttInfo}`);
    }
  });
  client.subscribe(mqttTopic, (err) => {
    if (!err) { 
      console.log(`Subscribed to ${mqttTopic}`);
    }
  });
})

client.on('message', (topic, message) => {
  switch (topic) {
    case mqttInfo:
      handleMonInfo(message);
      break;
    case mqttTopic:
      handleMon(message);
      break;
    default:
    console.log(`Nothing to do. Received: ${message} on topic ${topic}`);
  }
})

function handleMonInfo(message) {
  console.log(message);
}

function handleMon(message){
  switch(message.toString('utf8')){
    case 'ON':
      //schedule payload
      let startdate = new Date(Date.now());
      startdate.setMinutes(startdate.getMinutes() + scheduleMin);
      schedulePayload(startdate);
      console.log(`Payload: ${message}\nScheduled at: ${startdate}\nMinutes: ${scheduleMin}`);
      break;
    case 'OFF':
      handleOff();
      break;
    default:
      console.error(`Invalid payload: ${message}`);
  }
}

function handleOff(){
  if(typeof job === 'undefined') {
    console.log('We haven\'t even started yet.');
  } else if (!job.nextInvocation()){
    console.log('Nothing planned.');
  }else {
    job.cancel()
    client.publish(mqttTopic, 'OFF');
    console.log('Cancelling active schedule. Sent payload: OFF. Confirmation will follow.');
  }
}

function schedulePayload(sdate){
  job = cron.scheduleJob(sdate, () => {
    client.publish(mqttTopic, 'OFF');
    console.log('Sent payload: OFF.');
    job.cancel();
  })
}